https://gitlab.com/Alex-Line/jse-06.git

# Task Manager JSE-06

## Developer
Aleksandr Linev

E-mail: uranus_123@mail.ru

## Software

* Maven 3.6.1
* Java 1.8
* JRE
* IDE IntelliJ IDEA CE

## Build commands

```
    mvn clean
```
```
    mvn install
```
## Run command
```
    java -jar target/jse-6.0-RELEASE.jar
```
