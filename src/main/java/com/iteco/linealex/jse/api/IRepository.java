package com.iteco.linealex.jse.api;

import com.iteco.linealex.jse.util.exception.InsetExistingEntityException;

import java.util.Collection;

public interface IRepository<T> {

    public Collection<T> findAll();

    public T findOne(String name);

    public T persist(T example) throws InsetExistingEntityException;

    public T merge(T example);

    public T remove(String name);

    public Collection<T> removeAll();

}
