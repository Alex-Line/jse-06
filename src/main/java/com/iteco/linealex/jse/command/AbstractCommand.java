package com.iteco.linealex.jse.command;

import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.enumerate.Role;
import com.iteco.linealex.jse.util.exception.UserIsNotLogInException;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public abstract class AbstractCommand {

    protected Bootstrap bootstrap;

    protected Scanner scanner;

    protected List<Role> roles = Arrays.asList(Role.ADMINISTRATOR, Role.ORDINARY_USER);

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;

    public abstract boolean secure();

    public List<Role> getAvailableRoles() {
        return roles;
    }

    public Bootstrap getBootstrap() {
        return bootstrap;
    }

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

}
