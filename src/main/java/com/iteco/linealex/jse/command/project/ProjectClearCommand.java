package com.iteco.linealex.jse.command.project;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;

import java.util.Collection;

public class ProjectClearCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "CLEAR THE LIST OF PROJECTS";
    }

    @Override
    public void execute() throws Exception {
        final User user = bootstrap.getUserService().getAuthorizedUser();
        if (user.getRole() == Role.ADMINISTRATOR) {
            final Collection<Project> collection = bootstrap.getProjectService().removeAllProjects();
            if (collection == null) return;
            System.out.println("[All PROJECTS REMOVED]\n");
            return;
        }
        final Collection<Project> userCollection = bootstrap.getProjectService().getAllProject(user.getId());
        if (userCollection == null) {
            System.out.println("[THERE IS NOT ANY " + user.getLogin() + "'s PROJECTS FOR REMOVING]\n");
            return;
        }
        for (Project project : userCollection) {
            bootstrap.getProjectService().removeProject(project.getName(), user.getId());
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}
