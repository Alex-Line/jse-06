package com.iteco.linealex.jse.command.project;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.util.DateFormatter;

import java.text.ParseException;

public class ProjectCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-create";
    }

    @Override
    public String description() {
        return "CREATE A NEW PROJECT AND SET SELECTION ON IT";
    }

    @Override
    public void execute() throws Exception {
        final User user = bootstrap.getUserService().getAuthorizedUser();
        final Project project = new Project();
        project.setUserId(user.getId());
        System.out.println("[ENTER PROJECT NAME]");
        final String projectName = scanner.nextLine().trim();
        project.setName(projectName);
        System.out.println("[ENTER PROJECT DESCRIPTION]");
        final String projectDescription = scanner.nextLine().trim();
        project.setDescription(projectDescription);
        System.out.println("[ENTER PROJECT START DATE IN FORMAT: DD.MM.YYYY]");
        project.setDateStart(DateFormatter.formatStringToDate(scanner.nextLine().trim()));
        System.out.println("[ENTER PROJECT FINISH DATE IN FORMAT: DD.MM.YYYY]");
        project.setDateFinish(DateFormatter.formatStringToDate(scanner.nextLine().trim()));
        final Project insertedProject = bootstrap.getProjectService().persist(project);
        if (insertedProject == null) {
            System.out.println("[THERE IS SOME NULL OR EMPTY DATA]");
            return;
        }
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
