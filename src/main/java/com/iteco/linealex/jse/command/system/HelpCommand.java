package com.iteco.linealex.jse.command.system;

import com.iteco.linealex.jse.command.AbstractCommand;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class HelpCommand extends AbstractCommand {

    private List<String> help = new ArrayList<>();

    @Override
    public String command() {
        return "help";
    }

    @Override
    public String description() {
        return "SHOW ALL AVAILABLE COMMANDS";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getCommands().forEach((nameOfTask, command) ->
                System.out.println(command.command() + " : " + command.description()));
    }

    @Override
    public boolean secure() {
        return false;
    }

}
