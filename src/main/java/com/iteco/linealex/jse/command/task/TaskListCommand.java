package com.iteco.linealex.jse.command.task;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.util.exception.UserIsNotLogInException;

import java.util.Collection;

public class TaskListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-list";
    }

    @Override
    public String description() {
        return "SHOW ALL TASKS IN THE SELECTED PROJECT OR WITHOUT IT";
    }

    @Override
    public void execute() throws Exception {
        final User user = bootstrap.getUserService().getAuthorizedUser();
        Collection<Task> collection = null;
        if (bootstrap.getProjectService().getSelectedProject() != null) {
            collection = bootstrap.getTaskService().getTasks(
                    bootstrap.getProjectService().getSelectedProject().getId(), user.getId());
        } else collection = bootstrap.getTaskService().getTasks(user.getId());
        if (collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANYTHING TO LIST]\n");
            return;
        }
        System.out.println("[TASK LIST]");
        int index = 1;
        for (Task task : collection) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println();
    }

    @Override
    public boolean secure() {
        return true;
    }

}
