package com.iteco.linealex.jse.command.task;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.util.exception.UserIsNotLogInException;

public class TaskRemoveCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String description() {
        return "REMOVE ONE TASK IN SELECTED PROJECT OR WITHOUT IT";
    }

    @Override
    public void execute() throws Exception {
        final User user = bootstrap.getUserService().getAuthorizedUser();
        System.out.println("[ENTER TASK NAME]");
        final String taskName = scanner.nextLine().trim();
        System.out.println("REMOVING TASK...");
        Task task = null;
        if (bootstrap.getProjectService().getSelectedProject() != null) {
            task = bootstrap.getTaskService().removeTask(bootstrap.getProjectService().getSelectedProject().getId(),
                    taskName, user.getId());
        } else task = bootstrap.getTaskService().removeTask(taskName, user.getId());
        if (task == null) {
            System.out.println("[THERE IS NOT SUCH TASK AS " + taskName
                    + ". PLEASE TRY AGAIN OR SELECT PROJECT FIRST]\n");
            return;
        }
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
