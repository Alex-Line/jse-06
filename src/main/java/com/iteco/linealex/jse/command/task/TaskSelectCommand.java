package com.iteco.linealex.jse.command.task;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.util.exception.UserIsNotLogInException;

public class TaskSelectCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-select";
    }

    @Override
    public String description() {
        return "SELECT A TASK FOR MANIPULATION FURTHER";
    }

    @Override
    public void execute() throws Exception {
        final User user = bootstrap.getUserService().getAuthorizedUser();
        System.out.println("[ENTER THE NAME OF TASK FOR SELECTION]");
        final String taskName = scanner.nextLine().trim();
        Task task = null;
        if (bootstrap.getProjectService().getSelectedProject() != null) {
            task = bootstrap.getTaskService().selectTask(bootstrap.getProjectService().getSelectedProject().getId(),
                    taskName, user.getId());
        } else task = bootstrap.getTaskService().selectTask(taskName, user.getId());
        if (task == null) {
            System.out.println("[THERE IS NOT SUCH TASK AS " + taskName
                    + ". PLEASE TRY TO SELECT AGAIN OR SELECT PROJECT FIRST]\n");
            return;
        }
        if (task.getProjectId() == null) {
            System.out.println("[WAS SELECTED WITHOUT PROJECT]");
            System.out.println(task + "\n");
        }
        System.out.println("[WAS SELECTED IN THE PROJECT WITH ID" + task.getProjectId() + "]");
        System.out.println(task + "\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
