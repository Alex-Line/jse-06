package com.iteco.linealex.jse.command.user;

import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.util.TransformatorToHashMD5;
import com.iteco.linealex.jse.util.exception.ShortPasswordException;


public class UserCreateCommand extends UserAbstractCommand {

    @Override
    public String command() {
        return "user-create";
    }

    @Override
    public String description() {
        return "REGISTER NEW USER IF THEY DO NOT EXIST. (AVAILABLE FOR AMDMINS ONLY)";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER USER LOGIN. IT MUST BE UNIQUE!");
        final String login = scanner.nextLine().trim();
        System.out.println("ENTER USER PASSWORD. IT MUST BE 8 DIGITS OR LONGER");
        final String password = scanner.nextLine().trim();
        if (password.length() < 8) throw new ShortPasswordException();
        final String hashPassword = TransformatorToHashMD5.getHash(password);
        User newUser = new User(login, hashPassword);
        newUser = bootstrap.getUserService().createUser(newUser);
        System.out.println("[WAS REGISTERED NEW USER]");
        System.out.println(newUser);
        System.out.println("[OK]\n");
    }

}
