package com.iteco.linealex.jse.command.user;

import com.iteco.linealex.jse.command.AbstractCommand;

public class UserLogOutCommand extends AbstractCommand {

    @Override
    public String command() {
        return "logout";
    }

    @Override
    public String description() {
        return "SIGN OUT OF YOUR ACCOUNT IF IT WAS SIGN IN";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getUserService().getAuthorizedUser() == null) {
            System.out.println("[YOU HAVE NOT BEEN SIGN IN]\n");
            return;
        }
        bootstrap.getUserService().signOutLog();
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}
