package com.iteco.linealex.jse.command.user;

import com.iteco.linealex.jse.entity.User;

public class UserShowCommand extends UserAbstractCommand {

    @Override
    public String command() {
        return "user-show";
    }

    @Override
    public String description() {
        return "SHOW ANY USER BY LOGIN. (AVAILABLE FOR AMDMINS ONLY)";
    }

    @Override
    public void execute() throws Exception {
        User authorizedUser = bootstrap.getUserService().getAuthorizedUser();
        if (authorizedUser == null) {
            System.out.println("[YOU MUST BE AUTHORISED TO DO THAT]\n");
            return;
        }
        System.out.println("ENTER USER LOGIN");
        final String login = scanner.nextLine().trim();
        if (login.equals(bootstrap.getUserService().getAuthorizedUser().getLogin())) {
            System.out.println(bootstrap.getUserService().getAuthorizedUser());
            return;
        }
        User user = bootstrap.getUserService().getUser(login);
        System.out.println(user);
        System.out.println();
    }

}
