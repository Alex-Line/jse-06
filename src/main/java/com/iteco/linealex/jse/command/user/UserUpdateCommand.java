package com.iteco.linealex.jse.command.user;

import com.iteco.linealex.jse.command.AbstractCommand;

public class UserUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-update";
    }

    @Override
    public String description() {
        return "UPDATE USER'S PASSWORD";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER OLD PASSWORD");
        final String oldPassword = scanner.nextLine().trim();
        System.out.println("ENTER NEW PASSWORD");
        final String newPassword = scanner.nextLine().trim();
        System.out.println("ENTER NEW PASSWORD AGAIN");
        if (newPassword.equals(scanner.nextLine().trim())) {
            bootstrap.getUserService().updateUserPassword(oldPassword, newPassword);
            System.out.println("[OK]\n");
        } else System.out.println("MISTAKE IN THE REPEATING OF NEW PASSWORD\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
