package com.iteco.linealex.jse.context;

import com.iteco.linealex.jse.command.*;
import com.iteco.linealex.jse.command.project.*;
import com.iteco.linealex.jse.command.system.ClearSelectionCommand;
import com.iteco.linealex.jse.command.system.ExitCommand;
import com.iteco.linealex.jse.command.system.HelpCommand;
import com.iteco.linealex.jse.command.task.*;
import com.iteco.linealex.jse.command.user.*;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;
import com.iteco.linealex.jse.repository.ProjectRepository;
import com.iteco.linealex.jse.repository.TaskRepository;
import com.iteco.linealex.jse.repository.UserRepository;
import com.iteco.linealex.jse.service.ProjectService;
import com.iteco.linealex.jse.service.TaskService;
import com.iteco.linealex.jse.service.UserService;
import com.iteco.linealex.jse.util.TransformatorToHashMD5;
import com.iteco.linealex.jse.util.exception.LowAccessLevelException;
import com.iteco.linealex.jse.util.exception.TaskManagerException;
import com.iteco.linealex.jse.util.exception.UserIsNotLogInException;

import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public final class Bootstrap {

    private static final Scanner scanner = new Scanner(System.in);

    private ProjectService projectService;

    private TaskService taskService;

    private ProjectRepository projectRepository;

    private TaskRepository taskRepository;

    private UserService userService;

    private UserRepository userRepository;

    private Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    {
        registry(new HelpCommand());
        registry(new ClearSelectionCommand());
        registry(new ExitCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveCommand());
        registry(new ProjectSelectCommand());
        registry(new TaskAttachCommand());
        registry(new TaskClearAllCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskRemoveCommand());
        registry(new TaskSelectCommand());
        registry(new TaskListCommand());
        registry(new UserCreateCommand());
        registry(new UserLogInCommand());
        registry(new UserListCommand());
        registry(new UserShowCommand());
        registry(new UserLogOutCommand());
        registry(new UserUpdateCommand());
    }

    public Bootstrap() {
        this.projectRepository = new ProjectRepository();
        this.taskRepository = new TaskRepository();
        this.userRepository = new UserRepository();
        this.taskService = new TaskService(taskRepository);
        this.projectService = new ProjectService(projectRepository);
        this.userService = new UserService(userRepository);
    }

    private void registry(AbstractCommand command) {
        final String commandName = command.command();
        if (commandName == null || commandName.isEmpty()) return;
        final String commandDescription = command.description();
        if (commandDescription == null || commandDescription.isEmpty()) return;
        command.setBootstrap(this);
        command.setScanner(scanner);
        commands.put(command.command(), command);
    }

    public void start() {
        try {
            final User admin = new User("admin", TransformatorToHashMD5.getHash("11111111"));
            admin.setRole(Role.ADMINISTRATOR);
            userRepository.persist(admin);
            userRepository.persist(new User("startuser", TransformatorToHashMD5.getHash("22222222")));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine().trim().toLowerCase();
            execute(command);
        }
    }

    private void execute(final String command) {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        try {
            if (abstractCommand.secure() && userService.getAuthorizedUser() == null)
                throw new UserIsNotLogInException();
            if (userService.getAuthorizedUser() != null &&
                    !abstractCommand.getAvailableRoles().contains(userService.getAuthorizedUser().getRole())) {
                System.out.println(abstractCommand.getAvailableRoles());
                System.out.println(userService.getAuthorizedUser().getRole());
                throw new LowAccessLevelException();
            }
            abstractCommand.execute();
        } catch (TaskManagerException taskException) {
            System.out.println(taskException.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public ProjectService getProjectService() {
        return projectService;
    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public void setProjectRepository(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    public void setCommands(Map<String, AbstractCommand> commands) {
        this.commands = commands;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

}