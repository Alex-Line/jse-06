package com.iteco.linealex.jse.entity;

import com.iteco.linealex.jse.enumerate.Role;

import java.util.UUID;

public class User {

    private String login;

    private String hashPassword;

    private Role role = Role.ORDINARY_USER;

    private final String id = UUID.randomUUID().toString();

    public User(String login, String hashPassword) {
        this.login = login;
        this.hashPassword = hashPassword;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHashPassword() {
        return hashPassword;
    }

    public void setHashPassword(String hashPassword) {
        this.hashPassword = hashPassword;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "User " + login +
                ",\n     role = " + role.getName() +
                ",\n     id = " + id +
                '}';
    }
}
