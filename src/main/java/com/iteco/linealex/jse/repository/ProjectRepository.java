package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.api.IRepository;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.util.exception.InsetExistingEntityException;

import java.util.*;

public class ProjectRepository implements IRepository<Project> {

    private final Map<String, Project> projects = new LinkedHashMap<>();

    public boolean contains(String name, String userId) {
        for (Map.Entry<String, Project> entry : projects.entrySet()) {
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (!entry.getValue().getName().equals(name)) continue;
            return true;
        }
        return false;
    }

    @Override
    public Collection<Project> findAll() {
        return projects.values();
    }

    public Collection<Project> findAll(String userId) {
        List<Project> list = new ArrayList<>();
        for (Map.Entry<String, Project> entry : projects.entrySet()) {
            if (!entry.getValue().getUserId().equals(userId)) continue;
            list.add(entry.getValue());
        }
        return list;
    }

    @Override
    public Project findOne(String name) {
        for (Map.Entry<String, Project> entry : projects.entrySet()) {
            if (entry.getValue().getName().equals(name)) {
                return entry.getValue();
            }
        }
        return null;
    }

    public Project findOne(String name, String userId) {
        for (Map.Entry<String, Project> entry : projects.entrySet()) {
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (entry.getValue().getName().equals(name)) {
                return entry.getValue();
            }
        }
        return null;
    }

    @Override
    public Project persist(Project example) throws InsetExistingEntityException {
        for (Map.Entry<String, Project> entry : projects.entrySet()) {
            if (entry.getValue().getName().equals(example.getName())) {
                throw new InsetExistingEntityException();
            }
        }
        projects.put(example.getId(), example);
        return example;
    }

    @Override
    public Project merge(Project example) {
        Project oldProject = findOne(example.getName(), example.getUserId());
        if (oldProject == null) return oldProject;
        oldProject.setName(example.getName());
        oldProject.setDescription(example.getDescription());
        oldProject.setDateStart(example.getDateStart());
        oldProject.setDateFinish(example.getDateFinish());
        return oldProject;
    }

    @Override
    public Project remove(String name) {
        Project project = null;
        for (Map.Entry<String, Project> entry : projects.entrySet()) {
            if (entry.getValue().getName().equals(name)) {
                project = entry.getValue();
                projects.remove(project.getId());
            }
        }
        return project;
    }

    public Project remove(String name, String userId) {
        Project project = null;
        for (Map.Entry<String, Project> entry : projects.entrySet()) {
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (entry.getValue().getName().equals(name)) {
                project = entry.getValue();
                projects.remove(project.getId());
            }
        }
        return project;
    }

    @Override
    public Collection<Project> removeAll() {
        Map<String, Project> initialMap = projects;
        projects.clear();
        return initialMap.values();
    }

}
