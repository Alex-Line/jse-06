package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.api.IRepository;
import com.iteco.linealex.jse.entity.Task;

import java.util.*;

public final class TaskRepository implements IRepository<Task> {

    private final Map<String, Task> tasks = new LinkedHashMap<>();

    @Override
    public Collection<Task> findAll() {
        Map<String, Task> map = new LinkedHashMap<>();
        for (Map.Entry<String, Task> entry : tasks.entrySet()) {
            if (entry.getValue().getProjectId() == null) {
                map.put(entry.getKey(), entry.getValue());
            }
        }
        return map.values();
    }

    public Collection<Task> findAll(String projectId) {
        Map<String, Task> map = new LinkedHashMap<>();
        for (Map.Entry<String, Task> entry : tasks.entrySet()) {
            if (entry.getValue().getProjectId() != null && entry.getValue().getProjectId().equals(projectId)) {
                map.put(entry.getKey(), entry.getValue());
            }
        }
        return map.values();
    }

    public Collection<Task> findAll(String projectId, String userId) {
        Map<String, Task> map = new LinkedHashMap<>();
        for (Map.Entry<String, Task> entry : tasks.entrySet()) {
            if (entry.getValue().getUserId() == null || !entry.getValue().getUserId().equals(userId)) continue;
            if (entry.getValue().getProjectId() != null && entry.getValue().getProjectId().equals(projectId)) {
                map.put(entry.getKey(), entry.getValue());
            }
        }
        return map.values();
    }

    @Override
    public Task findOne(String taskName) {
        Task task = null;
        for (Map.Entry<String, Task> entry : tasks.entrySet()) {
            if (entry.getValue().getName().equals(taskName)) continue;
            if (entry.getValue().getProjectId() != null) continue;
            task = entry.getValue();
        }
        return task;
    }

    public Task findOne(String taskName, String projectId) {
        Task task = null;
        for (Map.Entry<String, Task> entry : tasks.entrySet()) {
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (entry.getValue().getProjectId() == null) continue;
            if (entry.getValue().getProjectId().equals(projectId)) continue;
            task = entry.getValue();
        }
        return task;
    }

    public Task findOne(String taskName, String projectId, String userId) {
        Task task = null;
        for (Map.Entry<String, Task> entry : tasks.entrySet()) {
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (entry.getValue().getProjectId() == null) continue;
            if (entry.getValue().getProjectId().equals(projectId)) continue;
            task = entry.getValue();
        }
        return task;
    }

    @Override
    public Task persist(Task example) {
        boolean contains = true;
        if (example.getProjectId() == null) {
            contains = contains(example.getProjectId(), example.getName());
        } else {
            contains = contains(example.getProjectId(), example.getName(), example.getUserId());
        }
        if (contains) return null;
        tasks.put(example.getId(), example);
        return example;
    }

    @Override
    public Task merge(Task example) {
        Task oldTask = findOne(example.getName(), example.getProjectId(), example.getUserId());
        if (oldTask == null) return oldTask;
        oldTask.setName(example.getName());
        oldTask.setDescription(example.getDescription());
        oldTask.setDateStart(example.getDateStart());
        oldTask.setDateFinish(example.getDateFinish());
        oldTask.setProjectId(example.getProjectId());
        oldTask.setUserId(example.getUserId());
        persist(example);
        return oldTask;
    }

    @Override
    public Task remove(String taskName) {
        for (Map.Entry<String, Task> entry : tasks.entrySet()) {
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (!(entry.getValue().getProjectId() == null)) continue;
            return tasks.remove(entry.getKey());
        }
        return null;
    }

    public Task remove(String taskName, String userId) {
        for (Map.Entry<String, Task> entry : tasks.entrySet()) {
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (!(entry.getValue().getProjectId() == null)) continue;
            return tasks.remove(entry.getKey());
        }
        return null;
    }

    public Task remove(String taskName, String projectId, String userId) {
        for (Map.Entry<String, Task> entry : tasks.entrySet()) {
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (entry.getValue().getProjectId() != null) continue;
            if (!entry.getValue().getProjectId().equals(projectId)) continue;
            return tasks.remove(entry.getKey());
        }
        return null;
    }

    @Override
    public Collection<Task> removeAll() {
        Map<String, Task> initialMap = new LinkedHashMap<>(tasks);
        tasks.clear();
        return initialMap.values();
    }

    public Collection<Task> removeAll(String userId) {
        List<Task> list = new ArrayList<>();
        for (Iterator<Map.Entry<String, Task>> iterator = tasks.entrySet().iterator(); iterator.hasNext(); ) {
            Map.Entry<String, Task> entry = iterator.next();
            if (entry.getValue().getUserId() != null && entry.getValue().getUserId().equals(userId)) {
                list.add(entry.getValue());
                iterator.remove();
            }
        }
        return list;
    }

    public boolean contains(String taskName, String userId) {
        for (Map.Entry<String, Task> entry : tasks.entrySet()) {
            if (entry.getValue().getProjectId() != null) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (!entry.getValue().getUserId().equals(userId)) continue;
            return true;
        }
        return false;
    }

    public boolean contains(String projectId, String taskName, String userId) {
        for (Map.Entry<String, Task> entry : tasks.entrySet()) {
            if (!entry.getValue().getProjectId().equals(projectId)) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (!entry.getValue().getUserId().equals(userId)) continue;
            return true;
        }
        return false;
    }

}
