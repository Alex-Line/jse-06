package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.api.IRepository;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.util.exception.InsetExistingEntityException;

import java.util.*;

public class UserRepository implements IRepository<User> {

    private Map<String, User> users = new LinkedHashMap<>();

    @Override
    public Collection<User> findAll() {
        return users.values();
    }

    @Override
    public User findOne(String login) {
        for (Map.Entry<String, User> entry : users.entrySet()) {
            if (!entry.getValue().getLogin().equals(login)) continue;
            return entry.getValue();
        }
        return null;
    }

    public User findOne(String login, String userId) {
        for (Map.Entry<String, User> entry : users.entrySet()) {
            if (!entry.getValue().getId().equals(userId)) continue;
            if (!entry.getValue().getLogin().equals(login)) continue;
            return entry.getValue();
        }
        return null;
    }

    public boolean contains(String login) {
        for (Map.Entry<String, User> entry : users.entrySet()) {
            if (!entry.getValue().getLogin().equals(login)) continue;
            return true;
        }
        return false;
    }

    @Override
    public User persist(User example) {
        users.put(example.getLogin(), example);
        return example;
    }

    @Override
    public User merge(User example) {
        User user = findOne(example.getLogin(), example.getId());
        if (user == null) return null;
        user.setRole(example.getRole());
        user.setHashPassword(example.getHashPassword());
        return user;
    }

    @Override
    public User remove(String login) {
        for (Map.Entry<String, User> entry : users.entrySet()) {
            if (!entry.getValue().equals(login)) continue;
            return entry.getValue();
        }
        return null;
    }

    @Override
    public Collection<User> removeAll() {
        List<User> removedUsers = new ArrayList(users.values());
        users.clear();
        return removedUsers;
    }

}
