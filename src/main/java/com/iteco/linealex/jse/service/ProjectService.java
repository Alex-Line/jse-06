package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.repository.ProjectRepository;
import com.iteco.linealex.jse.util.exception.InsetExistingEntityException;

import java.util.Collection;

public class ProjectService {

    private Project selectedProject = null;

    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project selectProject(String projectName, String userId) {
        selectedProject = null;
        if (projectName == null || projectName.isEmpty()) return selectedProject;
        selectedProject = getProjectByName(projectName, userId);
        return selectedProject;
    }

    public Project createProject(String projectName, String userId) throws InsetExistingEntityException {
        Project project = null;
        if (projectName == null || projectName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        selectedProject = null;
        if (!projectRepository.contains(projectName, userId)) {
            project = new Project();
            project.setName(projectName);
            project.setUserId(userId);
            projectRepository.persist(project);
            selectedProject = project;
        }
        return project;
    }

    public Project persist(Project project) throws InsetExistingEntityException {
        if (project == null) return null;
        if (project.getName() == null || project.getName().isEmpty()) return null;
        if (project.getDescription() == null || project.getDescription().isEmpty()) return null;
        if (project.getUserId() == null || project.getUserId().isEmpty()) return null;
        return projectRepository.persist(project);
    }

    public Collection<Project> getAllProject() {
        return projectRepository.findAll();
    }

    public Collection<Project> getAllProject(String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findAll(userId);
    }

    public Project removeProject(String projectName) {
        if (projectName == null || projectName.isEmpty()) return null;
        Project project = projectRepository.remove(projectName);
        if (project != null) selectedProject = null;
        return project;
    }

    public Project removeProject(String projectName, String userId) {
        if (projectName == null || projectName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        Project project = projectRepository.remove(projectName, userId);
        if (project != null) selectedProject = null;
        return project;
    }

    public Collection<Project> removeAllProjects() {
        return projectRepository.removeAll();
    }

    public void clearSelection() {
        selectedProject = null;
    }

    public Project getProjectByName(String projectName, String userId) {
        if (projectName == null || projectName.isEmpty()) return null;
        return projectRepository.findOne(projectName, userId);
    }

    public Project getSelectedProject() {
        return selectedProject;
    }

}
