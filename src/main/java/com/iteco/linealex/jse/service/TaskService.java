package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.repository.TaskRepository;

import java.util.Collection;
import java.util.Iterator;

public class TaskService {

    private TaskRepository taskRepository;

    private Task selectedTask = null;


    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task selectTask(String projectId, String taskName, String userId) {
        if (projectId == null || projectId.isEmpty()) selectedTask = selectTask(taskName, userId);
        if (taskName == null || taskName.isEmpty()) return null;
        selectedTask = taskRepository.findOne(taskName, projectId);
        return selectedTask;
    }

    public Task selectTask(String taskName, String userId) {
        if (taskName == null || taskName.isEmpty()) return null;
        selectedTask = taskRepository.findOne(taskName, userId);
        return selectedTask;
    }

    public Task createTask(String taskName, String userId) {
        if (userId == null || userId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        Task task = new Task();
        task.setName(taskName);
        task.setUserId(userId);
        selectedTask = task;
        task = taskRepository.persist(task);
        return task;
    }

    public Task createTask(String projectId, String taskName, String userId) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return createTask(taskName, userId);
        if (taskName == null || taskName.isEmpty()) return null;
        Task task = new Task();
        task.setName(taskName);
        task.setProjectId(projectId);
        task.setUserId(userId);
        selectedTask = task;
        task = taskRepository.persist(task);
        return task;
    }

    public Collection<Task> getTasks() {
        return taskRepository.findAll();
    }

    public Collection<Task> getTasks(String userId) {
        return taskRepository.findAll();
    }

    public Collection<Task> getTasks(String projectId, String userId) {
        if (projectId == null || projectId.isEmpty()) return getTasks(userId);
        return taskRepository.findAll(projectId, userId);
    }

    public Task removeTask(String taskName, String userId) {
        if (taskName == null || taskName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return taskRepository.remove(taskName, userId);
    }

    public Task removeTask(String projectId, String taskName, String userId) {
        if (projectId == null || projectId.isEmpty()) return removeTask(taskName, userId);
        if (taskName == null || taskName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return taskRepository.remove(taskName, projectId, userId);
    }

    public Collection<Task> removeAllTasksFromProject(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        Collection<Task> collection = taskRepository.findAll(projectId);
        for (Task task : collection) {
            taskRepository.remove(task.getName());
        }
        return collection;
    }

    public Collection<Task> removeAllTasksFromProject(String projectId, String userId) {
        if (projectId == null || projectId.isEmpty()) return null;
        Collection<Task> collection = taskRepository.findAll(projectId, userId);
        for (Task task : collection) {
            taskRepository.remove(task.getName());
        }
        return collection;
    }

    public Collection<Task> removeAllTasks(String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return taskRepository.removeAll(userId);
    }

    public void clearSelection() {
        selectedTask = null;
    }

    public boolean attachTaskToProject(String projectId) {
        if (projectId == null || projectId.isEmpty()) return false;
        for (Task task : taskRepository.findAll()) {
            if (task.getProjectId() != null) continue;
            if (!task.getId().equals(selectedTask)) continue;
            task.setProjectId(projectId);
            return true;
        }
        return false;
    }

    public Task getSelectedTask() {
        return selectedTask;
    }

}
