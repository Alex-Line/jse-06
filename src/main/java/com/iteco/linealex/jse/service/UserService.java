package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;
import com.iteco.linealex.jse.repository.UserRepository;
import com.iteco.linealex.jse.util.*;
import com.iteco.linealex.jse.util.exception.*;

import java.util.Collection;


public class UserService {

    private UserRepository userRepository;

    private User authorizedUser = null;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User signInUser(String login, String password) throws Exception {
        if (authorizedUser != null) {
            signOutLog();
            return null;
        }
        if (login == null || login.isEmpty()) throw new LoginIncorrectException();
        if (!userRepository.contains(login)) throw new UserIsNotExistException();
        if (password == null || password.isEmpty()) throw new WrongPasswordException();
        String hashPassword = TransformatorToHashMD5.getHash(password);
        User user = userRepository.findOne(login);
        if (!user.getHashPassword().equals(hashPassword)) throw new WrongPasswordException();
        authorizedUser = user;
        return authorizedUser;
    }

    public User signOutLog() {
        if (authorizedUser == null) return authorizedUser;
        User user = authorizedUser;
        authorizedUser = null;
        return user;
    }

    public User createUser(User user) throws TaskManagerException {
        if (authorizedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        if (user == null) return null;
        if (userRepository.contains(user.getLogin())) throw new InsetExistingEntityException();
        authorizedUser = userRepository.persist(user);
        return authorizedUser;
    }

    public User updateUserPassword(String oldPassword, String newPassword) throws Exception {
        if (authorizedUser == null) throw new UserIsNotLogInException();
        if (oldPassword == null || oldPassword.isEmpty()) throw new WrongPasswordException();
        String hashOldPassword = TransformatorToHashMD5.getHash(oldPassword);
        if (!authorizedUser.getHashPassword().equals(hashOldPassword)) throw new WrongPasswordException();
        if (newPassword == null || newPassword.isEmpty()) throw new WrongPasswordException();
        if (newPassword.length() < 8) throw new ShortPasswordException();
        String hashNewPassword = TransformatorToHashMD5.getHash(newPassword);
        authorizedUser.setHashPassword(hashNewPassword);
        return authorizedUser;
    }

    public User getUser() {
        return authorizedUser;
    }

    public User getUser(String login) throws TaskManagerException {
        if (authorizedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        if (authorizedUser == null) throw new UserIsNotLogInException();
        if (login == null || login.isEmpty()) throw new LoginIncorrectException();
        User user = userRepository.findOne(login);
        if (user == null) throw new UserIsNotExistException();
        return user;
    }

    public Collection<User> getUsers() throws LowAccessLevelException {
        if (authorizedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        return userRepository.findAll();
    }

    public User getAuthorizedUser() {
        return authorizedUser;
    }

}
