package com.iteco.linealex.jse.util.exception;

public class LoginIncorrectException extends TaskManagerException {

    @Override
    public String getMessage() {
        return "YOU ENTER WRONG OR EMPTY LOGIN. COMMAND WAS INTERRUPTED\n";
    }

}
