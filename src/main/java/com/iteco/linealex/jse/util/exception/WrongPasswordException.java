package com.iteco.linealex.jse.util.exception;

public class WrongPasswordException extends TaskManagerException {

    @Override
    public String getMessage() {
        return "YOU ENTER WRONG OR EMPTY PASSWORD. COMMAND WAS INTERRUPTED\n";
    }

}
